﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutofacBootstrapper
{
	public static class Bootstrapper
	{		
		//private static readonly ILog _Logger = LogProvider.GetCurrentClassLogger();

		public static IContainer AutofacContainer = null;

		public static IContainer Initialize()
		{
			//_Logger.Info("Bootstrapper.Initialize");
			//EntityFramework_Plus_ProLicense.Activate();

			return Bootstrapper.Initialize(builder =>
			{
				//AutoMapperConfig.RegisterForDI(builder);
				//QuartzAutofacConfig.RegisterForDI(builder);
			});
		}

		private static IContainer Initialize(Action<ContainerBuilder> containerBuilder)
		{
			AutofacContainer = AutofacLoader.Initialize(containerBuilder);
			return AutofacContainer;
		}
	}
}

﻿using Autofac;
using SomeService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutofacBootstrapper
{
	public static class AutofacLoader
	{
		public static IContainer Initialize(Action<ContainerBuilder> containerBuilder)
		{
			IContainer container = null;
			ContainerBuilder builder = new ContainerBuilder();

			// http://stackoverflow.com/a/23221833/54159
			//builder.RegisterAssemblyTypes(typeof(ISuperService).Assembly)
			//	.Where(t => typeof(ISuperService).IsAssignableFrom(t))
			//	.AsImplementedInterfaces();

			//builder.RegisterAssemblyTypes(typeof(ISuperService).Assembly)
			//	.Where(t => typeof(ISubWuselComponent).IsAssignableFrom(t))
			//	.AsImplementedInterfaces();


			// register all interfaces with implemented interfaces
			builder.RegisterAssemblyTypes(typeof(ISuperService).Assembly)				
				.AsImplementedInterfaces();

			//builder.RegisterAssemblyTypes(typeof(ISuperService).Assembly)
			//	.Where(t => t.Name.EndsWith("Service"))
			//	.AsImplementedInterfaces();
			//	//.As(t => t.GetInterfaces().FirstOrDefault(
			//	//	i => i.Name == "I" + t.Name));

			if (containerBuilder != null)
			{
				containerBuilder(builder);
			}

			container = builder.Build();

			return container;
		}
	}
}

using System;
using System.Collections.Generic;
using System.Linq;

namespace SomeService
{
	public interface ISubWuselComponent
	{
		string GetSuperString();
		string SomeNewElement { get; set; }
		string WuselText { get; set; }
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SomeService
{
	public interface IPublicData
	{
		string Name { get; set; }
	}

	public class PublicData : IPublicData
	{
		public string Name { get; set; }
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SomeService
{

	internal class SuperService : ISuperService
	{
		private readonly ISubWuselComponent _SubWuselComponent;

		public SuperService(ISubWuselComponent subWuselComponent)
		{
			_SubWuselComponent = subWuselComponent;
		}

		public void Execute()
		{
			var internalOnly = new InternallyOnly();

			Console.WriteLine("Execute");
			Console.WriteLine("From subComponent: {0}", _SubWuselComponent.WuselText);
			Console.WriteLine("From subComponent new: {0}", _SubWuselComponent.GetSuperString());

			internalOnly.DoStuff();
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SomeService
{

	internal class SubWuselComponent : ISubWuselComponent
	{
		public string WuselText { get; set; }
		public string SomeNewElement { get; set; }

		public SubWuselComponent()
		{
			WuselText      = "WuselDusel";
			SomeNewElement = "something new";
		}

		public string GetSuperString()
		{
			return WuselText + " - " + SomeNewElement;
		}

	}
}

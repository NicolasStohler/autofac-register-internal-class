using System;
using System.Collections.Generic;
using System.Linq;

namespace SomeService
{
	public interface ISuperService
	{
		void Execute();
	}
}

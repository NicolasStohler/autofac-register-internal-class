﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutofacBootstrapper;
using Autofac;
using Autofac.Core;

namespace App_console
{
	class Program
	{
		static void Main(string[] args)
		{
			var autofacContainer = Bootstrapper.Initialize();

			try
			{
				using (var scope = autofacContainer.BeginLifetimeScope())
				{
					var services = scope.ComponentRegistry.Registrations.SelectMany(x => x.Services)
							   .OfType<IServiceWithType>()
							   .Select(x => x.ServiceType);

					//var pd = new SomeService.PublicData();
					//var x = new SomeService.SubWuselComponent();

					var serviceInstance = scope.Resolve<SomeService.ISuperService>();
					serviceInstance.Execute();
				}
			}
			catch(Exception ex)
			{
				Console.WriteLine("{0}: {1}", ex.GetType().Name, ex.Message);
				throw;
			}
			Console.WriteLine("done.");
			Console.ReadLine();
		}
	}
}
